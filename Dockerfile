FROM python:3

WORKDIR /usr/src/app
RUN pip install --no-cache-dir prometheus_client python-dateutil python-geohash aiohttp

EXPOSE 9000

ENV API_URL="Leuven@http://api.luftdaten.info/v1/filter/area=50.87917530044147,4.699573516845703,1.236 Eeklo@http://api.luftdaten.info/v1/filter/area=51.185029917182874,3.5866928100585938,3.721" \
    API_INTERVAL=150 \
    API_TIMESHIFT=300

COPY ./luftdaten.py /usr/src/app

CMD ["python", "luftdaten.py"]
