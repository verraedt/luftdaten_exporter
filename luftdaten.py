#!/usr/bin/env python3

from prometheus_client import start_http_server, Gauge
from dateutil import tz
import asyncio
import async_timeout
import aiohttp
import datetime
import json
import geohash
import logging
import os
import time
import threading
import urllib.request
import queue

# Url to parse
API_URL = os.getenv('API_URL', "Leuven@http://api.luftdaten.info/v1/filter/area=50.87917530044147,4.699573516845703,1.236 Eeklo@http://api.luftdaten.info/v1/filter/area=51.185029917182874,3.5866928100585938,3.721")

# Interval in seconds to probe api
API_INTERVAL = int(os.getenv('API_INTERVAL', 150))

# Time shift in seconds to apply to api results
API_TIMESHIFT = int(os.getenv('API_TIMESHIFT', 300))

# Timeout to apply to api results
API_TIMEOUT = int(os.getenv('API_TIMEOUT', 30))

# Metric lifetime in seconds, metric will be available this many seconds
METRIC_LIFETIME = int(os.getenv('METRIC_LIFETIME', 300))

# Metrics
labels = ['sensor', 'sensor_type', 'manufacturer', 'location', 'latitude', 'longitude', 'altitude', 'geohash', 'country', 'place']

METRICS = {
    'humidity': Gauge('luftdaten_humidity', 'Humidity', labels),
    'temperature': Gauge('luftdaten_temperature', 'Temperature', labels),
    'P1': Gauge('luftdaten_p1', 'Air Quality PM10', labels),
    'P2': Gauge('luftdaten_p2', 'Air Quality PM2.5', labels),
    'humidity_unixtime': Gauge('luftdaten_humidity_unixtime', 'Timestamp of latest update of Humidity', labels),
    'temperature_unixtime': Gauge('luftdaten_temperature_unixtime', 'Timestamp of latest update of Temperature', labels),
    'P1_unixtime': Gauge('luftdaten_p1_unixtime', 'Timestamp of latest update of Air Quality PM10', labels),
    'P2_unixtime': Gauge('luftdaten_p2_unixtime', 'Timestamp of latest update of Air Quality PM2.5', labels),
}

async def get_url(url, timeout):
#    req = urllib.request.urlopen(url, timeout=timeout)
#    data = req.read()
#    encoding = req.info().get_content_charset('utf-8')
#    return json.loads(data.decode(encoding))

    async with aiohttp.ClientSession() as session:
        with async_timeout.timeout(timeout):
            async with session.get(url) as response:
                if response.status == 200:
                    data = await response.read()
                    return json.loads(data)
                else:
                    return Exception('Response status', response.status)

async def fill_queue(queue, data, timeshift, loc = ''):
    count = 0
    for item in data:
        timestamp = datetime.datetime.strptime(item['timestamp'], '%Y-%m-%d %H:%M:%S') + datetime.timedelta(seconds=timeshift)

        geo = geohash.encode(float(item['location']['latitude']), float(item['location']['longitude']), 8)

        labels = [
            item['sensor']['id'],
            item['sensor']['sensor_type']['name'],
            item['sensor']['sensor_type']['manufacturer'],
            item['location']['id'],
            item['location']['latitude'],
            item['location']['longitude'],
            item['location']['altitude'],
            geo,
            item['location']['country'],
            loc,
        ]

        for value in item['sensordatavalues']:
            if value['value_type'] in METRICS:
                await queue.put((timestamp, value['value_type'], labels, float(value['value'])))
                count = count + 1
            else:
                logger.info('Unknown metric %s', value['value_type'])

    logger.info('Queued %d metrics', count)

async def fetcher(queue, url, timeshift, interval, timeout, loc = ''):
    # Infinite loop
    while True:
        now = datetime.datetime.utcnow()

        # Get url and fill queue
        try:
            logger.info('Fetching url %s', url)
            data = await get_url(url, timeout)
            await fill_queue(queue, data, timeshift, loc)

            # Wait until next update
            delta = now + datetime.timedelta(seconds=interval) - datetime.datetime.utcnow()
            await asyncio.sleep(delta.total_seconds())
        except asyncio.TimeoutError:
            logger.info('Timeout occurred while fetching')
        except Exception as e:
            logger.info('Exception occurred while fetching - %s', e)

async def dispatcher(queue, lifetime):
    logger.info('Dispatcher started')

    tzutc = tz.tzutc()
    tzloc = tz.tzlocal()

    # Infinite loop
    while True:
        try:
            event = await queue.get()
            timestamp, metric, labels, value = event
        
            now = datetime.datetime.utcnow()
            
            delay = (now - timestamp).total_seconds()

            if delay >= 0:
                if value is not False:
                    logger.info('Dispatching metric %5s/%-11s at %s %s', labels[0], metric, timestamp, '(%s seconds late)' % int(delay) if delay >= 0.5 else '')
                    METRICS[metric].labels(*labels).set(value)
                    METRICS[metric + '_unixtime'].labels(*labels).set(timestamp.replace(tzinfo=tzutc).astimezone(tzloc).timestamp())

                    await queue.put(((timestamp + datetime.timedelta(seconds=lifetime)), metric, labels, False))
                else:
                    max_age = (timestamp - datetime.timedelta(seconds=lifetime)).replace(tzinfo=tzutc).astimezone(tzloc).timestamp()

                    if METRICS[metric + '_unixtime'].labels(*labels)._value.get() <= max_age:
                        logger.info('Removing stale metric %5s/%-11s', labels[0], metric)

                        try:
                            METRICS[metric].remove(*labels)
                            METRICS[metric + '_unixtime'].remove(*labels)
                        except KeyError:
                            pass
            else:
                await queue.put((timestamp, metric, labels, value))

            queue.task_done()
            if delay < 0:
                await asyncio.sleep(delay * -0.5)
        
        except Exception as e:
            logger.info('Exception occurred while dispatching - %s', e)

if __name__ == '__main__':
    logger = logging.getLogger(__name__)
    logging.basicConfig(level=logging.DEBUG, format="%(message)s")

    loop = asyncio.get_event_loop()

    # Start up the server to expose the metrics.
    start_http_server(9000)

    queue = asyncio.PriorityQueue(loop=loop)

    for url in API_URL.split(' '):
        loc = ''
        if '@' in url:
            loc, url = url.split('@')
        asyncio.ensure_future(fetcher(queue, url, API_TIMESHIFT, API_INTERVAL, API_TIMEOUT, loc))

    asyncio.ensure_future(dispatcher(queue, METRIC_LIFETIME))

    try:
        loop.run_forever()
    finally:
        loop.close()
